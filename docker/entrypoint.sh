#!/bin/bash

# Fail on errors.
set -e

if [ "$1" == "pip" ]; then
    python3 -m "$@"
else
    python3 run.py "$@"
fi
