==============================
Create an Idem Provider Plugin
==============================

You can create an Idem Provider Plugin to manage a currently unsupported cloud
or anything that provides an API. An Idem Provider Plugin can have many
subsystems to help you manage your cloud, but generally, most Provider Plugins
will have an "Acct plugin", "Exec modules", "State modules", and a "Tool
plugin".

Depending on the use case, a Provider Plugin might also have other plugins,
such as an ":doc:`ESM plugin </topics/explanation/enforced_state_management>`"
or ":doc:`Reconciliation plugin </topics/reference/reconciliation_loop>`".

----

Provider Plugin Overview
========================


Acct plugin
+++++++++++

An acct plugin handles authentication with your API. You can have multiple acct
plugins to handle different ways of authenticating with your API. For example,
the idem-aws provider plugin has acct plugins to handle authentication with
IAM, gsuite as well as aws cli authentication. `idem-aws acct plugins
<https://gitlab.com/vmware/idem/idem-aws/-/tree/master/idem_aws/acct/aws>`_

By default, acct expects credential information to be kept in an encrypted yaml
file. A good default location is in
`$HOME/.config/idem/credentials.yaml.fernet` or where `$XDG_CONFIG_HOME` is
configured. More details can be found in the :doc:`acct description
<./acct-plugin>`.

----

Exec Modules
++++++++++++

Exec modules, also sometimes referred to as Execution modules, are the python
modules where most of the actual work gets done. Exec modules are exposed via
Idem CLI. Most Exec modules are imperative functions that interact directly
with your API. For example, if you have an Exec module that interacts with your
API to manage VMs you might have functions similar to the following:

.. list-table:: VM Exec functions
   :header-rows: 1

   * - Function
     - Description
   * - get
     - Get details about a specific VM
   * - list
     - List all VMs
   * - create
     - Create a new VM
   * - destroy
     - Destroy a specific VM
   * - start
     - Start a specific VM
   * - stop
     - Stop a specific VM

Here's an example of how you might use an Exec module function with the Idem
CLI:

.. code-block:: bash

    idem exec my_cloud.vm.list

----

State Modules
+++++++++++++

State modules are python modules that ensure your desired state of resources
through idempotent functions. A State module function will use your cloud
provider API to ensure the actual state matches the desired state.

For example, if you have a State module that interacts with your API to manage
VMs you might have functions similar to the following:

.. list-table:: VM State functions
   :header-rows: 1

   * - Function
     - Description
   * - present
     - Ensure a specific VM exists
   * - absent
     - Ensure a specific VM does not exist
   * - describe
     - Return sls yaml that describes the actual current state of your cloud VMs


Generally, a State module won't interact with your API directly. A well-behaved
State module will use the Exec modules to check the current state of the API
and then use that information to decide if any changes are needed. If changes
are needed, the State module will use the Exec module functions to make the
necessary changes.

While State module functions can be run from the Idem CLI, generally you will
use yaml files with an `.sls` extension to define how you want your resource
configured. These `sls` files can be stored wherever you desire. The path used
here is just an example. Here's a basic example of how an `sls` file might
look:

.. code-block:: yaml
   :caption: /srv/idem/sls/webserver.sls

    webserver_prod:
      my_cloud.vm.present:
        - name: webserver01
        - image_id: ubuntu/22.04
        - instance_type: t2.nano
        - tags:
            Name: prod webserver
            datacenter: Dallas


Once we have defined how we want the VM configured in our `sls` file we can use
the `idem state` CLI command to make sure our VM is configured accordingly in
the cloud.  Remember, since `idem state` is idempotent, the first time it is
executed the VM will be created in the cloud. Every consecutive run it will
only be updated, if needed.

.. code-block:: bash

    idem state /srv/idem/sls/webserver.sls

----


Tool plugin
+++++++++++

A Tool plugin is an internal python module that provides utility type
functionality for other modules within plugin code and is not exposed via CLI.
Tool plugins typically provide utility functions that convert API responses to
Idem format or that compare API responses with the desired state to determine
if an update is needed.

Tool plugin functions are only used internally to Idem.

----

Initiate a New Idem Provider Plugin
===================================

.. grid:: 1

    .. grid-item-card:: Create Provider Plugin
        :link: create-provider-plugin
        :link-type: doc


        Provider Plugin

        :bdg-info:`Provider Plugin`

Standard Provider Plugin subsystems
+++++++++++++++++++++++++++++++++++

.. grid:: 2

    .. grid-item-card:: Exec Modules
        :link: exec-modules
        :link-type: doc


        Exec Modules

        :bdg-info:`Exec Modules`

    .. grid-item-card:: State Modules
        :link: state-modules
        :link-type: doc


        State Modules

        :bdg-info:`State Modules`

    .. grid-item-card:: Acct Plugin
        :link: acct-plugin
        :link-type: doc


        Acct Plugin

        :bdg-info:`Acct Plugin`

    .. grid-item-card:: Tool Plugin
        :link: tool-plugin
        :link-type: doc


        Tool Plugin

        :bdg-info:`Tool Plugin`



.. toctree::
   :maxdepth: 1
   :glob:
   :hidden:

   create-provider-plugin
   exec-modules
   state-modules
   acct-plugin
   tool-plugin
