===========
Idem 16.0.0
===========

Idem 16.0.0 implements `evbus <https://pypi.org/project/pop-evbus>`_ in idem cli

Writing an Ingress Plugin
=========================

Refer to the pop-evbus repository for how to write an `ingress plugin <https://gitlab.com/vmware/idem/evbus/-/blob/master/docs/topics/ingress_plugins.rst>`__

Setting up Credentials
======================

Configured profiles are formatted as follows:

.. code-block:: sls

    provider:
      profile_name:
          profile_data:

The `profile` parameter for the idem event `put` and `put_nowait` functions specifies which profile_name should
be used for firing an event.
If no profile is specified, the profiles called "default" will be used.
There can be multiple providers with the same profile name,
the event will be propagated to all providers that have a matching profile name.
A context (ctx) will be generated that will be sent to the appropriate ingress plugin's publish function
based on `profile`.


Firing Events
=============

Here are ways to fire events.

From Code
---------

The ``body`` is any serializable data that comprises the main part of the event
The ``profile`` is the ingress profile from ``acct`` that this event should be published to.

Asynchronous put:

.. code-block:: python

    async def my_func(hub):
        await hub.idem.event.put(body={"message": "event content"}, profile="default")


Synchronous put:

.. code-block:: python

    def my_func(hub):
        hub.idem.event.put_nowait(body={"message": "event content"}, profile="default")

From Jinja/SLS
--------------

Events can also be fired from within an idem sls file via jinja:

.. code-block:: sls

    {%- hub.idem.event.put_nowait(body={"message": "event content"}, profile="default") %}

CLI
===

You can fire one-off events from the CLI like so:

.. code-block:: bash

    idem exec test.event ingress_profile="default" body="my_event" --serialize-plugin="json"

Testing
=======

Create a credentials.yml file for connecting to local kafka/rabbitmq containers:

.. code-block:: sls

    pika:
      test_development_evbus_pika:
        connection:
          host: localhost
          port: 5672
          login: guest
          password: guest
        routing_key: my_test_routing_key
    kafka:
      test_development_evbus_kafka:
        connection:
          bootstrap_servers: localhost:9092
        topics:
          - my_test_topic

Encrypt the credentials file and set the ACCT environment variables

.. code-block:: bash

    $ export ACCT_KEY=$(idem encrypt credentials.yml)
    $ export ACCT_FILE="$PWD/credentials.yml.fernet"


Start a local rabbitmq server to run the tests:

.. code-block:: bash

    $ docker run -p 5672:5672 \
      --env RABBITMQ_HOSTS=rabbitmq \
      --env RABBITMQ_PORT=5672 \
      --env RABBITMQ_USER=guest \
      --env RABBITMQ_PASS=guest \
      --env RABBITMQ_PROTOCOL=amqp \
      rabbitmq:management

Start a local kafka server to run the tests:

.. code-block:: bash

    $ docker run -p 2181:2181 -p 443:9092 -p 9092:9092 \
      --env ADVERTISED_LISTENERS=PLAINTEXT://localhost:443,INTERNAL://localhost:9093 \
      --env LISTENERS=PLAINTEXT://0.0.0.0:9092,INTERNAL://0.0.0.0:9093 \
      --env SECURITY_PROTOCOL_MAP=PLAINTEXT:PLAINTEXT,INTERNAL:PLAINTEXT \
      --env INTER_BROKER=INTERNAL \
      krisgeus/docker-kafka

Install the idem test requirements:

.. code-block:: bash

    $ pip install -r requirements/test.in

Run the tests with pytest:

.. code-block:: bash

    $ pytest tests

Logging Handler
===============

idem adds support for a new logging handler, the queue.
All log messages will be published as events to ingress queues with a ``idem-logger`` profile.

.. code-block:: bash

    idem exec test.event ingress_profile="idem-logger" body="my_event" --log-level=debug --log-handler=event
