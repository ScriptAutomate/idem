import pytest_idem.runner as runner


def test_describe(hub):
    ctx = {}

    with runner.named_tempfile(delete=True) as fh:
        hub.exec.tests.auto.ITEMS = hub.idem.managed.file_dict(str(fh))
        # Auto-populate some values for the auto state describe
        hub.exec.tests.auto.create(ctx, "name", kw1=1, kw2=2)
        d = hub.exec.tests.auto.get(ctx, "name")
        assert d
        assert d.ret == {"name": "name", "resource_id": "name", "kw1": 1, "kw2": 2}

        ret = hub.pop.Loop.run_until_complete(hub.idem.describe.run("tests.auto"))

    returned_states = list(ret.values())
    assert returned_states, "No states were returned"
    assert returned_states[0] == {
        "tests.auto.present": [
            {"name": "name"},
            {"resource_id": "name"},
            {"kw1": 1},
            {"kw2": 2},
        ]
    }


def test_sls(hub, code_dir, mock_time, tmpdir):
    name = "test"
    render = "yaml"
    cache_dir = tmpdir
    sls_dir = code_dir / "tests" / "sls"
    sls_sources = [f"file://{sls_dir}"]

    with runner.named_tempfile() as fh:
        hub.exec.tests.auto.ITEMS = hub.idem.managed.file_dict(str(fh))
        hub.pop.Loop.run_until_complete(
            hub.idem.state.apply(
                name,
                sls_sources,
                render,
                "serial",
                ["states"],
                cache_dir,
                ["auto_state"],
                test=False,
                acct_file=None,
                acct_key=None,
                acct_data={"profiles": {"test": {"default": {}}}},
                managed_state={},
            )
        )

    errors = hub.idem.RUNS[name]["errors"]
    assert not errors, errors

    ret = hub.idem.RUNS[name]["running"]

    assert ret["tests.auto_|-test-autostate-absent_|-foo_|-absent"] == {
        "__run_num": 5,
        "changes": {},
        "comment": ["'tests.auto:foo' already absent"],
        "esm_tag": "tests.auto_|-test-autostate-absent_|-foo_|-",
        "name": "foo",
        "new_state": {},
        "old_state": {},
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "start_time": str(mock_time),
        "__id__": "test-autostate-absent",
        "tag": "tests.auto_|-test-autostate-absent_|-foo_|-absent",
        "total_seconds": 0.0,
        "ref": "states.auto_state.absent",
        "acct_details": {},
    }
    assert ret["tests.auto_|-test-autostate-create_|-foo_|-present"] == {
        "__run_num": 1,
        "changes": {
            "new": {"kw1": None, "name": "foo", "param": 1, "resource_id": "foo"},
            "old": {},
        },
        "comment": [
            "Created 'tests.auto:foo'",
            "Updated 'tests.auto:foo'",
        ],
        "esm_tag": "tests.auto_|-test-autostate-create_|-foo_|-",
        "name": "foo",
        "new_state": {"kw1": None, "name": "foo", "param": 1, "resource_id": "foo"},
        "old_state": {},
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "start_time": str(mock_time),
        "__id__": "test-autostate-create",
        "tag": "tests.auto_|-test-autostate-create_|-foo_|-present",
        "total_seconds": 0.0,
        "ref": "states.auto_state.present",
        "acct_details": {},
    }
    assert ret["tests.auto_|-test-autostate-delete_|-foo_|-absent"] == {
        "__run_num": 4,
        "changes": {
            "new": {},
            "old": {"kw1": None, "name": "foo", "param": 2, "resource_id": "foo"},
        },
        "comment": ["Deleted 'tests.auto:foo'"],
        "esm_tag": "tests.auto_|-test-autostate-delete_|-foo_|-",
        "name": "foo",
        "new_state": {},
        "old_state": {"kw1": None, "name": "foo", "param": 2, "resource_id": "foo"},
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "start_time": str(mock_time),
        "__id__": "test-autostate-delete",
        "tag": "tests.auto_|-test-autostate-delete_|-foo_|-absent",
        "total_seconds": 0.0,
        "ref": "states.auto_state.absent",
        "acct_details": {},
    }

    assert ret["tests.auto_|-test-autostate-present_|-foo_|-present"] == {
        "__run_num": 2,
        "changes": {},
        "comment": ["'tests.auto:foo' already exists"],
        "esm_tag": "tests.auto_|-test-autostate-present_|-foo_|-",
        "name": "foo",
        "new_state": {"kw1": None, "name": "foo", "param": 1, "resource_id": "foo"},
        "old_state": {"kw1": None, "name": "foo", "param": 1, "resource_id": "foo"},
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "start_time": str(mock_time),
        "__id__": "test-autostate-present",
        "tag": "tests.auto_|-test-autostate-present_|-foo_|-present",
        "total_seconds": 0.0,
        "ref": "states.auto_state.present",
        "acct_details": {},
    }
    assert ret["tests.auto_|-test-autostate-update_|-foo_|-present"] == {
        "__run_num": 3,
        "changes": {"new": {"param": 2}, "old": {"param": 1}},
        "comment": [
            "'tests.auto:foo' already exists",
            "Updated 'tests.auto:foo'",
        ],
        "esm_tag": "tests.auto_|-test-autostate-update_|-foo_|-",
        "name": "foo",
        "new_state": {"kw1": None, "name": "foo", "param": 2, "resource_id": "foo"},
        "old_state": {"kw1": None, "name": "foo", "param": 1, "resource_id": "foo"},
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "start_time": str(mock_time),
        "__id__": "test-autostate-update",
        "tag": "tests.auto_|-test-autostate-update_|-foo_|-present",
        "total_seconds": 0.0,
        "ref": "states.auto_state.present",
        "acct_details": {},
    }


def test_exec(hub):
    with runner.named_tempfile(delete=True) as fh:
        hub.exec.tests.auto.ITEMS = hub.idem.managed.file_dict(fh)
        obj = dict(foo=1, bar=2, baz=3)
        name = "taco cat"
        ctx = {}

        ret = hub.exec.tests.auto.get(ctx, name)
        assert not ret, "already exists"

        ret = hub.exec.tests.auto.create(ctx, name, **obj)
        assert ret, "Could not create"
        assert "Created" in ret.comment

        ret = hub.exec.tests.auto.get(ctx, name)
        assert ret, "does not exist"
        assert ret.ret == {
            "bar": 2,
            "baz": 3,
            "foo": 1,
            "kw1": None,
            "name": "taco cat",
            "resource_id": "taco cat",
        }

        ret = hub.exec.tests.auto.update(ctx=ctx, name=name, resource_id=name, foo=0)
        assert ret, "Could not update"
        assert "Updated" in ret.comment

        ret = hub.exec.tests.auto.get(ctx, name)
        assert ret, "does not exist"
        assert ret.ret == {
            "bar": 2,
            "baz": 3,
            "foo": 0,
            "kw1": None,
            "name": "taco cat",
            "resource_id": "taco cat",
        }

        ret = hub.exec.tests.auto.delete(ctx, name, name, **obj)
        assert ret, "Could not delete"

        ret = hub.exec.tests.auto.get(ctx, name)
        assert not ret.ret, "still exists"
