first thing:
  test.succeed_with_changes

second thing:
  test.succeed_without_changes

arg bind:
  test.succeed_with_arg_bind:
    - arg_bind:
      - test:
        - first thing:
          - testing:new: parameters:test1
        - third thing:
          - testing:new: parameters:test2

third thing:
  test.succeed_with_changes

indexed arg:
  test.succeed_with_arg_bind:
    - parameters:
        tests:
          - test: dev
          - test:
    - arg_bind:
      - test:
        - first thing:
          - tests[0][0]:new: parameters:tests[1]:test

fail no new_state:
  test.succeed_with_arg_bind:
    - arg_bind:
      - test:
        - second thing:
          - testing:new: parameters:test

fail arg not found:
  test.succeed_with_arg_bind:
    - arg_bind:
      - test:
        - first thing:
          - testing:arg_not_found: parameters:arg_not_found

fail arg index not found:
  test.succeed_with_arg_bind:
    - parameters:
        tests:
          - test: dev
    - arg_bind:
      - test:
        - first thing:
          - tests[0][0]:new: parameters:tests[1]:test

fail referenced arg index not found:
  test.succeed_with_arg_bind:
    - arg_bind:
      - test:
        - first thing:
          - tests[0][5]:new: parameters:tests:test
