test_state:
  test.present:
    - result: True
    - new_state:
        integer: 10
        float: 10.1
        boolean: True


test_arg_bind:
  test.present:
    - new_state:
        name_with_int: "hello ${test:test_state:integer}"
        name_with_float: "hello ${test:test_state:float}"
        name_with_bool: "hello ${test:test_state:boolean}"
