test_dataclass_flat:
  test_dataclass.present:
    - arg_dc_flat:
        ArgRequiredBool: False
        ArgWithNone: value
        arg_inner:
          InnerRequiredBool: True
          InnerBoolTrue: True
          InnerBoolFalse: True

test_dataclass_flat_missing_inner:
  test_dataclass.present:
    - arg_dc_flat:
        ArgRequiredBool: True
        ArgWithNone: newValue

test_dataclass_flat_missing_required_negative:
  test_dataclass.present:
    - arg_dc_flat:
        ArgWithNone: value
        arg_inner:
          InnerRequiredBool: True
          InnerBoolTrue: True
          InnerBoolFalse: True

test_dataclass_flat_missing_required_inner_negative:
  test_dataclass.present:
    - arg_dc_flat:
        ArgRequiredBool: False
        ArgWithNone: value
        arg_inner:
          InnerBoolTrue: True
          InnerBoolFalse: True

test_dataclass_list:
    test_dataclass.present:
       - arg_dc_list:
          - ArgRequiredStr: test-str-1
            ArgRequiredInt: 3
            ArgRequiredBool: False
          - ArgRequiredStr: test-str-2
            ArgRequiredInt: 33
            ArgRequiredBool: True
            ArgDefaultInt: 133

test_dataclass_list_missing_required_negative:
    test_dataclass.present:
       - arg_dc_list:
          - ArgRequiredStr: hello

test_dataclass_only_required_combo:
    test_dataclass.present:
      - arg_dc_list:
          - ArgRequiredStr: test-str-1
            ArgRequiredInt: 3
            ArgRequiredBool: False
          - ArgRequiredStr: test-str-2
            ArgRequiredInt: 33
            ArgRequiredBool: True
            ArgDefaultInt: 133
      - arg_dc_flat:
          ArgRequiredBool: False
          ArgWithNone: value
          arg_inner:
            InnerRequiredBool: True
            InnerBoolTrue: True
            InnerBoolFalse: True

test_dataclass_inner_tree:
  test_dataclass.present:
    - arg_dc_default_in_inner:
        arg_inner:
          InnerNone: InnerInt_default_20
        arg_inner_different_default:
          InnerNone: InnerInt_default_40

test_dataclass_list_within_list:
  test_dataclass.present:
    - arg_dc_list:
      - ArgRequiredStr: test-str-2
        ArgRequiredInt: 3
        ArgRequiredBool: False
        arg_inner_list:
          - InnerListRequiredStr: inner-1
          - InnerListRequiredStr: inner-2

test_dataclass_list_within_list_negative:
  test_dataclass.present:
    - arg_dc_list:
      - ArgRequiredStr: test-str-2
        ArgRequiredInt: 3
        ArgRequiredBool: False
        arg_inner_list:
          - InnerListBoolFalse: True
          - InnerListRequiredStr: inner-2
