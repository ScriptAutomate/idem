test_invert_state_1:
  test.present:
    - resource_id: idem-test-1
    - new_state:
        resource_id: idem-test-1
        nested_params:
           param1: val1
           param2: val2
           param3: val3
    - result: true

sls_run_state_include:
  sls.run:
    - sls_sources:
        - "./nested_include.sls"
    - nested_params: ${test:test_invert_state_1:nested_params}
