dep-1:
  test.unique_op:
    - require:
      - test: ind-1
      - test: ind-2

dep-2:
  test.unique_op:
    - require:
      - test: ind-1
      - test: ind-2

dep-3:
  test.unique_op:
    - require:
      - test: ind-1
      - test: ind-2
      - test: ind-3

ind-1:
  test.unique_op

ind-2:
  test.unique_op:
    - require:
      - test: ind-3

ind-3:
  test.nop
