__contracts__ = ["soft_fail"]


def error(hub):
    raise ValueError("This gets transformed into an ExecReturn with a failure")
