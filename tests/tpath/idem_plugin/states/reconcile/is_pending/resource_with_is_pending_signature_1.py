# test_reconcile with a signature: is_pending(hub, ret: dict, state: str = None, **pending_kwargs)

__contracts__ = ["resource"]


def present(hub, ctx, name: str, number_of_reruns: int = 0):
    """
    Returns successful with execution counter

    name:
        A unique string.
    """
    execution_count = (
        ctx.get("rerun_data", {}).get("execution_count", 0)
        if ctx.get("rerun_data")
        else 0
    )
    execution_count += 1

    return {
        "name": name,
        "result": True,
        "rerun_data": {"execution_count": execution_count},
        "comment": "test",
        "old_state": {},
        "new_state": {"number_of_reruns": number_of_reruns},
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_is_pending_signature_1.present": []}}


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs) -> bool:
    if ret.get("new_state", {}).get("number_of_reruns", 0) >= ret.get(
        "rerun_data", {}
    ).get("execution_count", 0):
        return True
    else:
        return False
