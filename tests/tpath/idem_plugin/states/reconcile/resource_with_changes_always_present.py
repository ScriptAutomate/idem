"""Reconciliation will stop after --max-pending-runs"""
__contracts__ = ["resource"]
__reconcile_wait__ = {"static": {"wait_in_seconds": 1}}


def present(hub, ctx, name: str, number_of_reruns: int = 0):
    """
    Returns failure in state execution
    """
    execution_count = (
        ctx.get("old_state", {}).get("execution_count", 0)
        if ctx.get("old_state")
        else 0
    )
    execution_count += 1

    return {
        "name": name,
        "result": False,
        "comment": "test",
        "old_state": {},
        "new_state": {
            "number_of_reruns": number_of_reruns,
            "execution_count": execution_count,
        },
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_clearing_rerun_data.present": []}}
